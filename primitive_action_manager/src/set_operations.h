#ifndef _SET_OPERATIONS_H_
#define _SET_OPERATIONS_H_

#include <set>
#include <vector>
#include <algorithm>



template<typename T>
inline
void
set_difference (std::set<T> const& set1,
                std::set<T> const& set2,
                std::set<T>& result)
{
  result.clear();
  std::set_difference (set1.begin(),
                       set1.end(),
                       set2.begin(),
                       set2.end(),
                       std::inserter (result, result.begin()));
}



template<typename T>
inline
void
set_difference (std::set<T> const& set1,
                std::set<T> const& set2,
                std::vector<T>& result)
{
  result.clear();
  result.reserve (set1.size());
  std::set_difference (set1.begin(),
                       set1.end(),
                       set2.begin(),
                       set2.end(),
                       std::inserter (result, result.begin()));
}



template<typename T>
inline
std::vector<T>
set_difference (std::set<T> const& set1,
                std::set<T> const& set2)
{
  std::vector<T> tmp;
  set_difference (set1, set2, tmp);
  return tmp;
}

#endif
