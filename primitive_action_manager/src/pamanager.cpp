#include "pamanager.hpp"
#include "ros/ros.h"

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <sound_play/SoundRequest.h>
#include <boost/format.hpp>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>



typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;



class go2Door
{
    ros::NodeHandle nh_;
    ros::Timer timer_;
    ros::Publisher update_pub_;

  public:
    go2Door() : nh_() , timer_ (nh_.createTimer (ros::Duration (1.0), &go2Door::timer_callback, this)),
      update_pub_ (nh_.advertise<geometry_msgs::PoseStamped> ("move_base_simple/goal", 1, true))
    {
      ROS_INFO_STREAM ("START Go2Door!");
    }
    ~go2Door()
    {
      timer_.stop();  // bug do ros
      ROS_INFO_STREAM ("STOP Go2Door");
      update_pub_.shutdown();
    }

    void
    timer_callback (const ros::TimerEvent& te = ros::TimerEvent())
    {
      const ros::Time& time = te.current_real;

      geometry_msgs::PoseStamped poseStamped;

      poseStamped.header.frame_id = "map";
      poseStamped.pose.position.x = 0.89837;
      poseStamped.pose.position.y = 2.19705;
      poseStamped.pose.position.z = 0.0;
      poseStamped.pose.orientation.x = 0.0;
      poseStamped.pose.orientation.y = 0.0;
      poseStamped.pose.orientation.z = -0.453332914824;
      poseStamped.pose.orientation.w =  0.891341274898;

      update_pub_.publish (poseStamped);
    }
};



class go2Bed
{
    ros::NodeHandle nh_;
    ros::Timer timer_;
    ros::Publisher update_pub_;

  public:
    go2Bed() : nh_() , timer_ (nh_.createTimer (ros::Duration (5.0), &go2Bed::timer_callback, this))
      //  update_pub_ (nh_.advertise<geometry_msgs::PoseStamped> ("move_base_simple/goal", 1, true))
    {
      ROS_INFO_STREAM ("START Go2Bed!");
    }
    ~go2Bed()
    {
      timer_.stop();  // bug do ros
      ROS_INFO_STREAM ("STOP Go2Bed");
      update_pub_.shutdown();
    }

    void
    timer_callback (const ros::TimerEvent& te = ros::TimerEvent())
    {
      const ros::Time& time = te.current_real;

      MoveBaseClient ac ("move_base", true);
      while (!ac.waitForServer (ros::Duration (5.0))) {
        ROS_INFO ("Waiting for the move_base action server to come up");
      }

      move_base_msgs::MoveBaseGoal goal;

      //we'll send a goal to the robot to move 1 meter forward
      goal.target_pose.header.frame_id = "map";
      goal.target_pose.header.stamp = ros::Time::now();

      goal.target_pose.pose.position.x = 1.4378;
      goal.target_pose.pose.position.y = -2.85033;
      goal.target_pose.pose.orientation.w = 0.129853954355;
      goal.target_pose.pose.orientation.z = 0.991533131337;

      ROS_INFO ("Sending goal");
      ac.sendGoal (goal);

      // poseStamped.header.frame_id = "map";
      // poseStamped.pose.position.x =  1.43780;
      // poseStamped.pose.position.y = -2.85033;
      // poseStamped.pose.position.z = 0.0;
      // poseStamped.pose.orientation.x = 0.0;
      // poseStamped.pose.orientation.y = 0.0;
      // poseStamped.pose.orientation.z = 0.991533131337;
      // poseStamped.pose.orientation.w = 0.129853954355;

      // update_pub_.publish (poseStamped);
    }
};



class go2Table
{
    ros::NodeHandle nh_;
    ros::Timer timer_;
    ros::Publisher update_pub_;

  public:
    go2Table() : nh_() , timer_ (nh_.createTimer (ros::Duration (1.0), &go2Table::timer_callback, this)),
      update_pub_ (nh_.advertise<geometry_msgs::PoseStamped> ("move_base_simple/goal", 1, true))
    {
      ROS_INFO_STREAM ("START Go2Table!");
    }
    ~go2Table()
    {
      timer_.stop();  // bug do ros
      ROS_INFO_STREAM ("STOP Go2Table");
      update_pub_.shutdown();
    }

    void
    timer_callback (const ros::TimerEvent& te = ros::TimerEvent())
    {
      const ros::Time& time = te.current_real;

      MoveBaseClient ac ("move_base", true);
      while (!ac.waitForServer (ros::Duration (5.0))) {
        ROS_INFO ("Waiting for the move_base action server to come up");
      }

      move_base_msgs::MoveBaseGoal goal;

      //we'll send a goal to the robot to move 1 meter forward
      goal.target_pose.header.frame_id = "map";
      goal.target_pose.header.stamp = ros::Time::now();

      goal.target_pose.pose.position.x =  6.35942;
      goal.target_pose.pose.position.y = -1.34058;
      goal.target_pose.pose.orientation.w = -0.737821473358;
      goal.target_pose.pose.orientation.z = 0.674995906248;

      ROS_INFO ("Sending goal");
      ac.sendGoal (goal);


      // geometry_msgs::PoseStamped poseStamped;

      // poseStamped.header.frame_id = "map";
      // poseStamped.pose.position.x =  6.35942;
      // poseStamped.pose.position.y = -1.34058;
      // poseStamped.pose.position.z = 0.0;
      // poseStamped.pose.orientation.x = 0.0;
      // poseStamped.pose.orientation.y = 0.0;
      // poseStamped.pose.orientation.z = -0.737821473358;
      // poseStamped.pose.orientation.w =  0.674995906248;

      // update_pub_.publish (poseStamped);
    }
};



class go2RefBox
{
    ros::NodeHandle nh_;
    ros::Timer timer_;
    ros::Publisher update_pub_;
    ros::Publisher sound_pub_;

  public:
    go2RefBox() : nh_() , timer_ (nh_.createTimer (ros::Duration (1.0), &go2RefBox::timer_callback, this)),
      update_pub_ (nh_.advertise<geometry_msgs::PoseStamped> ("move_base_simple/goal", 1, true)),
      sound_pub_ (nh_.advertise<sound_play::SoundRequest> ("robotsound", 1, true))
    {
      ROS_INFO_STREAM ("START Go2Refbox!");
      sound_play::SoundRequest aux_msg;
      aux_msg.sound = -3;
      aux_msg.command = 2;

      std::string text = "'Going to the RefBox!'";
      aux_msg.arg = text;
      aux_msg.arg2 = "";
      sound_pub_.publish (aux_msg);
    }
    ~go2RefBox()
    {
      timer_.stop();  // bug do ros
      ROS_INFO_STREAM ("STOP Go2Refbox");
      update_pub_.shutdown();
    }

    void
    timer_callback (const ros::TimerEvent& te = ros::TimerEvent())
    {
      const ros::Time& time = te.current_real;

      MoveBaseClient ac ("move_base", true);
      while (!ac.waitForServer (ros::Duration (5.0))) {
        ROS_INFO ("Waiting for the move_base action server to come up");
      }

      move_base_msgs::MoveBaseGoal goal;

      //we'll send a goal to the robot to move 1 meter forward
      goal.target_pose.header.frame_id = "map";
      goal.target_pose.header.stamp = ros::Time::now();

      goal.target_pose.pose.position.x = 7.96607;
      goal.target_pose.pose.position.y = -4.65841;
      goal.target_pose.pose.orientation.w = -0.725259341335;
      goal.target_pose.pose.orientation.z = 0.688475771401;

      ROS_INFO ("Sending goal");
      ac.sendGoal (goal);

      // geometry_msgs::PoseStamped poseStamped;

      // poseStamped.header.frame_id = "map";
      // poseStamped.pose.position.x =  7.96607;
      // poseStamped.pose.position.y = -4.65841;
      // poseStamped.pose.position.z = 0.0;
      // poseStamped.pose.orientation.x = 0.0;
      // poseStamped.pose.orientation.y = 0.0;
      // poseStamped.pose.orientation.z = -0.725259341335;
      // poseStamped.pose.orientation.w =  0.688475771401;

      // update_pub_.publish (poseStamped);
    }
};



class go2Entrance
{
    ros::NodeHandle nh_;
    ros::Timer timer_;
    ros::Publisher update_pub_;
    ros::Publisher sound_pub_;

  public:
    go2Entrance() : nh_() , timer_ (nh_.createTimer (ros::Duration (1.0), &go2Entrance::timer_callback, this)),
      update_pub_ (nh_.advertise<geometry_msgs::PoseStamped> ("move_base_simple/goal", 1, true)),
      sound_pub_ (nh_.advertise<sound_play::SoundRequest> ("robotsound", 1, true))
    {
      ROS_INFO_STREAM ("START Go2Entrance!");
      sound_play::SoundRequest aux_msg;
      aux_msg.sound = -3;
      aux_msg.command = 2;

      std::string text = "'Going to the entrance!'";
      aux_msg.arg = text;
      aux_msg.arg2 = "";
      sound_pub_.publish (aux_msg);
    }
    ~go2Entrance()
    {
      timer_.stop();  // bug do ros
      ROS_INFO_STREAM ("STOP Go2Entrance");
      update_pub_.shutdown();
    }

    void
    timer_callback (const ros::TimerEvent& te = ros::TimerEvent())
    {
      const ros::Time& time = te.current_real;

      MoveBaseClient ac ("move_base", true);
      while (!ac.waitForServer (ros::Duration (5.0))) {
        ROS_INFO ("Waiting for the move_base action server to come up");
      }

      move_base_msgs::MoveBaseGoal goal;

      //we'll send a goal to the robot to move 1 meter forward
      goal.target_pose.header.frame_id = "map";
      goal.target_pose.header.stamp = ros::Time::now();

      goal.target_pose.pose.position.x = 3.53307;
      goal.target_pose.pose.position.y = -1.82674;
      goal.target_pose.pose.orientation.w = -0.140750921856;
      goal.target_pose.pose.orientation.z = 0.990045038368;

      ROS_INFO ("Sending goal");
      ac.sendGoal (goal);

      // geometry_msgs::PoseStamped poseStamped;

      // poseStamped.header.frame_id = "map";
      // poseStamped.pose.position.x =  3.53307;
      // poseStamped.pose.position.y = -1.82673;
      // poseStamped.pose.position.z = 0.0;
      // poseStamped.pose.orientation.x = 0.0;
      // poseStamped.pose.orientation.y = 0.0;
      // poseStamped.pose.orientation.z = -0.140750921856;
      // poseStamped.pose.orientation.w =  0.990045038368;

      // update_pub_.publish (poseStamped);
    }
};



class go2Elevator
{
    ros::NodeHandle nh_;
    ros::Timer timer_;
    ros::Publisher update_pub_;

  public:
    go2Elevator() : nh_() , timer_ (nh_.createTimer (ros::Duration (1.0), &go2Elevator::timer_callback, this)),
      update_pub_ (nh_.advertise<geometry_msgs::PoseStamped> ("move_base_simple/goal", 1, true))
    {
      ROS_INFO_STREAM ("START Go2Elevator!");
    }
    ~go2Elevator()
    {
      timer_.stop();  // bug do ros
      ROS_INFO_STREAM ("STOP Go2Elevator");
      update_pub_.shutdown();
    }

    void
    timer_callback (const ros::TimerEvent& te = ros::TimerEvent())
    {
      const ros::Time& time = te.current_real;

      geometry_msgs::PoseStamped poseStamped;

      poseStamped.header.frame_id = "map";
      poseStamped.pose.position.x = 2.432857;
      poseStamped.pose.position.y = -1.48659;
      poseStamped.pose.position.z = 0.0;
      poseStamped.pose.orientation.x = 0.0;
      poseStamped.pose.orientation.y = 0.0;
      poseStamped.pose.orientation.z = -0.0730773120065;
      poseStamped.pose.orientation.w =  0.997326278843;

      update_pub_.publish (poseStamped);

      //      ROS_INFO_STREAM ("TESTE Go2ElevatorHallway!");
    }
};



class go2LRM
{
    ros::NodeHandle nh_;
    ros::Publisher update_pub_;
    ros::Timer timer_;

  public:
    go2LRM() : nh_() , timer_ (nh_.createTimer (ros::Duration (1.0 / 30), &go2LRM::timer_callback, this)),
      update_pub_ (nh_.advertise<geometry_msgs::PoseStamped> ("move_base_simple/goal", 1, true))
    {
      ROS_INFO_STREAM ("START Go2LRM!");

      geometry_msgs::PoseStamped poseStamped;

      poseStamped.header.frame_id = "map";
      poseStamped.pose.position.x = -10.2591276169;
      poseStamped.pose.position.y = -1.54371643;
      poseStamped.pose.position.z = 0.0;
      poseStamped.pose.orientation.x = 0.0;
      poseStamped.pose.orientation.y = 0.0;
      poseStamped.pose.orientation.z = 0.680260630509;
      poseStamped.pose.orientation.w = 0.73297030948;

      update_pub_.publish (poseStamped);
    }

    ~go2LRM()
    {
      timer_.stop();  // bug do ros
      ROS_INFO_STREAM ("STOP Go2LRM");
      update_pub_.shutdown();
    }

    void
    timer_callback (const ros::TimerEvent& te = ros::TimerEvent())
    {
      const ros::Time& time = te.current_real;
      //     ROS_INFO_STREAM ("TESTE Go2LRM!");
    }
};



class go2CoffeeRoom
{
    ros::NodeHandle nh_;
    ros::Publisher update_pub_;
    ros::Timer timer_;

  public:
    go2CoffeeRoom() : nh_() , timer_ (nh_.createTimer (ros::Duration (1.0 / 10), &go2CoffeeRoom::timer_callback, this)),
      update_pub_ (nh_.advertise<geometry_msgs::PoseStamped> ("move_base_simple/goal", 1, true))
    {
      ROS_INFO_STREAM ("START Go2CoffeeRoom!");
    }

    ~go2CoffeeRoom()
    {
      timer_.stop();  // bug do ros
      ROS_INFO_STREAM ("STOP Go2CoffeeRoom");
      update_pub_.shutdown();
    }

    void
    timer_callback (const ros::TimerEvent& te = ros::TimerEvent())
    {
      const ros::Time& time = te.current_real;
      geometry_msgs::PoseStamped poseStamped;

      poseStamped.header.frame_id = "map";
      poseStamped.pose.position.x = -5.2418756485;
      poseStamped.pose.position.y = 11.0837287903;
      poseStamped.pose.position.z = 0.0;
      poseStamped.pose.orientation.x = 0.0;
      poseStamped.pose.orientation.y = 0.0;
      poseStamped.pose.orientation.z = 1.0;
      poseStamped.pose.orientation.w = 0.0;

      update_pub_.publish (poseStamped);

      //    ROS_INFO_STREAM ("TESTE Go2CoffeeRoom!");
    }
};



class go2SoccerField
{
    ros::NodeHandle nh_;
    ros::Publisher update_pub_;
    ros::Timer timer_;

  public:
    go2SoccerField() : nh_() , timer_ (nh_.createTimer (ros::Duration (1.0 / 10), &go2SoccerField::timer_callback, this)),
      update_pub_ (nh_.advertise<geometry_msgs::PoseStamped> ("move_base_simple/goal", 1, true))
    {
      ROS_INFO_STREAM ("START Go2SoccerField!");
    }

    ~go2SoccerField()
    {
      timer_.stop();  // bug do ros
      ROS_INFO_STREAM ("STOP Go2SoccerField");
      update_pub_.shutdown();
    }

    void
    timer_callback (const ros::TimerEvent& te = ros::TimerEvent())
    {
      const ros::Time& time = te.current_real;
      geometry_msgs::PoseStamped poseStamped;

      poseStamped.header.frame_id = "map";
      poseStamped.pose.position.x = -4.37555918;
      poseStamped.pose.position.y = -8.751221;
      poseStamped.pose.position.z = 0.0;
      poseStamped.pose.orientation.x = 0.0;
      poseStamped.pose.orientation.y = 0.0;
      poseStamped.pose.orientation.z = 0.393377617824;
      poseStamped.pose.orientation.w = 0.919376990029;

      update_pub_.publish (poseStamped);
      // ROS_INFO_STREAM ("TESTE Go2SoccerField!");
    }
};



class ttsCurrPosition
{
    ros::NodeHandle nh_;
    ros::Subscriber pose_pub_;
    ros::Publisher update_pub_;
    bool speak_once_;
  public:
    ttsCurrPosition() :
      nh_(),
      pose_pub_ (nh_.subscribe ("amcl_pose", 1, &ttsCurrPosition::poseCallback, this)),
      update_pub_ (nh_.advertise<sound_play::SoundRequest> ("robotsound", 1, true)),
      speak_once_ (true)
    {
      ROS_INFO_STREAM ("START ttsCurrPosition");
    }

    void poseCallback (const geometry_msgs::PoseWithCovarianceStamped& msg)
    {
      if (speak_once_) {
        sound_play::SoundRequest aux_msg;
        aux_msg.sound = -3;
        aux_msg.command = 2;

        std::string x = str (boost::format ("%1$.3f") % msg.pose.pose.position.x);
        std::string y = str (boost::format ("%1$.3f") % msg.pose.pose.position.y);

        std::string text = "'I am in " + x + " x and " + y + " y!'";

        aux_msg.arg = text;
        aux_msg.arg2 = "";
        update_pub_.publish (aux_msg);

        speak_once_ = false;
      }
    }

    ~ttsCurrPosition()
    {
      ROS_INFO_STREAM ("STOP ttsCurrPosition");
      update_pub_.shutdown();
    }
};



class ttsMoving
{
    ros::NodeHandle nh_;
    ros::Subscriber pose_pub_;
    ros::Publisher update_pub_;
    bool speak_once_;
  public:
    ttsMoving() :
      nh_(),
      pose_pub_ (nh_.subscribe ("move_base/current_goal", 1, &ttsMoving::goalposeCallback, this)),
      update_pub_ (nh_.advertise<sound_play::SoundRequest> ("robotsound", 1, true)),
      speak_once_ (true)
    {
      ROS_INFO_STREAM ("START ttsMoving");
    }

    void goalposeCallback (const geometry_msgs::PoseStamped& msg)
    {
      if (speak_once_) {
        sound_play::SoundRequest aux_msg;
        aux_msg.sound = -3;
        aux_msg.command = 2;

        std::string x = str (boost::format ("%1$.3f") % msg.pose.position.x);
        std::string y = str (boost::format ("%1$.3f") % msg.pose.position.y);

        std::string text = "'I am going to the" + x + " x and " + y + " y!'";

        aux_msg.arg = text;
        aux_msg.arg2 = "";
        update_pub_.publish (aux_msg);
        speak_once_ = false;
      }
    }

    ~ttsMoving()
    {
      ROS_INFO_STREAM ("STOP ttsMoving");
      update_pub_.shutdown();
    }
};



class ttsLRM
{
    ros::NodeHandle nh_;
    ros::Publisher update_pub_;
    bool speak_once_;
  public:
    ttsLRM() :
      nh_(),
      update_pub_ (nh_.advertise<sound_play::SoundRequest> ("robotsound", 1, true)),
      speak_once_ (true)
    {
      ROS_INFO_STREAM ("START ttsLRM");
      sound_play::SoundRequest aux_msg;
      aux_msg.sound = -3;
      aux_msg.command = 2;

      std::string text = "'I am over the predefined LRM position'";
      ROS_WARN_STREAM (text);
      aux_msg.arg = text;
      aux_msg.arg2 = "";
      update_pub_.publish (aux_msg);
    }

    ~ttsLRM()
    {
      ROS_INFO_STREAM ("STOP ttsLRM");
      update_pub_.shutdown();
    }
};



class ttsElevator
{
    ros::NodeHandle nh_;
    ros::Publisher update_pub_;
    bool speak_once_;
  public:
    ttsElevator() :
      nh_(),
      update_pub_ (nh_.advertise<sound_play::SoundRequest> ("robotsound", 1, true)),
      speak_once_ (true)
    {
      ROS_INFO_STREAM ("START ttsElevator");
      sound_play::SoundRequest aux_msg;
      aux_msg.sound = -3;
      aux_msg.command = 2;

      std::string text = "'I am near the Elevator'";
      ROS_WARN_STREAM (text);

      aux_msg.arg = text;
      aux_msg.arg2 = "";
      update_pub_.publish (aux_msg);
    }

    ~ttsElevator()
    {
      ROS_INFO_STREAM ("STOP ttsElevator");
      update_pub_.shutdown();
    }
};



class ttsCoffee
{
    ros::NodeHandle nh_;
    ros::Publisher update_pub_;
    bool speak_once_;
  public:
    ttsCoffee() :
      nh_(),
      update_pub_ (nh_.advertise<sound_play::SoundRequest> ("robotsound", 1, true)),
      speak_once_ (true)
    {
      ROS_INFO_STREAM ("START ttsLRM");
      sound_play::SoundRequest aux_msg;
      aux_msg.sound = -3;
      aux_msg.command = 2;

      std::string text = "'I am near the coffee machine'";
      ROS_WARN_STREAM (text);

      aux_msg.arg = text;
      aux_msg.arg2 = "";
      update_pub_.publish (aux_msg);
    }

    ~ttsCoffee()
    {
      ROS_INFO_STREAM ("STOP ttsCoffee");
      update_pub_.shutdown();
    }
};



int main (int argc, char** argv)
{
  ros::init (argc, argv, "primitive_action_manager");

  primitive_action_manager::Manager pamanager (primitive_action_manager::ManagerOptions ("Default")
      //Manager pamanager (ManagerOptions ("robot1")
      .primitive <go2SoccerField> ("go2SoccerField")
      .primitive <go2Elevator> ("go2Elevator")
      .primitive <ttsMoving> ("ttsMoving")
      .primitive <ttsCurrPosition> ("ttsCurrPosition")
      .primitive <go2CoffeeRoom> ("go2CoffeeRoom")
      .primitive <go2LRM> ("go2LRM")
      .primitive <ttsLRM> ("ttsLRM")
      .primitive <ttsCoffee> ("ttsCoffee")
      .primitive <ttsElevator> ("ttsElevator")
      .primitive <go2Bed> ("go2Bed")
      .primitive <go2Entrance> ("go2Entrance")
      .primitive <go2RefBox> ("go2RefBox")
      .primitive <go2Table> ("go2Table"));

  ros::spin();
}
