#ifndef __PA_MANAGER__
#define __PA_MANAGER__

#include <memory>
#include <stdexcept>
#include <vector>
#include <boost/utility.hpp>

#include "ros/ros.h"

#include "primitive_action_manager/PAInfo.h"
#include "primitive_action_manager/PAInfoMap.h"
#include "primitive_action_manager/PAUpdate.h"

#include "set_operations.h"



namespace primitive_action_manager
{
  typedef uint32_t Id;

  /**
   * @brief
   */
  class PrimitiveTypeBase
  {
    public:
      typedef std::shared_ptr<PrimitiveTypeBase> Ptr;
      typedef std::shared_ptr<PrimitiveTypeBase const> ConstPtr;

      virtual void start() = 0;
      virtual void stop() = 0;
  };

  /**
   * @brief
   */
  template<typename T> class PrimitiveType
    : public PrimitiveTypeBase
  {
      std::shared_ptr<T> running_;

    public:
      void start()
      {
        if (running_) {
          return;
        }
        running_ = std::make_shared<T>();
      }
      void stop()
      {
        running_.reset();
      }
  };


  /**
   * @brief Options to be used when constructing the Primitive Action Manager
   *
   * This class is a simple container of options, used by the Manager
   * constructor.
   */
  class ManagerOptions : boost::noncopyable
  {
    public:
      /**
       * @brief Constructor
       *
       * The constructor takes only one argument that must be always specified.
       *
       * @param robotName The robot Name, should be equal to the "color" of the token in the petri net
       */
      ManagerOptions (const std::string& robotName) :
        robotName_ (robotName) {};

      template<typename T>
      ManagerOptions& primitive (const std::string& name)
      {
        primitives_[name] = std::make_shared<PrimitiveType<T>>();
        return *this;
      }

    private:
      friend class Manager;

      std::map<std::string, PrimitiveTypeBase::Ptr> primitives_;
      const std::string robotName_;
  };

  /**
   * @brief Main class of the Primitive Action Manager
   *
   * This class is the Primitive Action Manager, responsible for the communication with the cortex (petrinet) and
   * the execution/preempt of primitive actions.
   */
  class Manager
  {
    public:
      /**
       * @brief Constructor
       *
       * The Manager constructor creates the Primitive Action Manager, evaluates if all the primitive actions
       * inserted are unique, creates the maps between (ids and names) and (names and primitives) and publishes
       * the PAInfoMap
       * @param options Options to be used by the Manager
       */
      Manager (const ManagerOptions& options) :
        robotName_ (options.robotName_),
        petrinet_sub_ (nh_.subscribe ("action_update", 1, &primitive_action_manager::Manager::cortexUpdateCallback, this))
      {
        id2primitive_.reserve (options.primitives_.size());
        id2name_.reserve (options.primitives_.size());

        for (auto it : options.primitives_) {
          std::string const name = it.first;

          if (primitiveActions_.count (name) != 0) {
            throw std::logic_error ("Action already on the Primitive Action Manager");
          }

          Id id = id2primitive_.size();
          primitiveActions_[name] = it.second;
          id2primitive_.push_back (it.second);
          id2name_.push_back (name);
          name2id_[name] = id;
        }

        pa_map_pub_ = nh_.advertise<primitive_action_manager::PAInfoMap> ("action_map", 1, true);
        publishActionMap();
      };

      ~Manager() {};


      /**
      * @brief Prints the set of running actions
      *
      * Prints as ROS_INFO_STREAM the set of the actions that are currently running
      */
      void PrintRunningActions()
      {
        if (runningActions_.empty()) {
          ROS_INFO_STREAM ("Running Actions is empty :(");
          return;
        }
        ROS_INFO_STREAM ("Running: ");
        for (auto const& id : runningActions_) {
          ROS_INFO_STREAM ("\tID: " << id << " Action: " << id2name_[id]);
        }
      }

    private:

      const std::string robotName_;
      ros::NodeHandle nh_;

      ros::Publisher pa_map_pub_;
      ros::Subscriber petrinet_sub_;

      std::map<std::string, PrimitiveTypeBase::Ptr> primitiveActions_;
      std::vector<PrimitiveTypeBase::Ptr> id2primitive_;
      std::vector<std::string> id2name_;
      std::map<std::string, Id> name2id_;

      std::set<Id> runningActions_;
      /**
       * @brief Callback for the subscriber of the cortex update topic
       *
       * This method is used to process the cortex update and execute/preempt the needed primitive actions.
       */
      void cortexUpdateCallback (const primitive_action_manager::PAUpdateConstPtr& msg)
      {
        if (msg->robot_id != robotName_) {
          ROS_INFO_STREAM ("msg:" <<  msg << "msg->robot_id: " << msg->robot_id << " robotName: " << robotName_);
          return;
        }

        std::set<Id> temp_runningActions;

        /* extract the actions */
        for (auto const& id : msg->execute) {
          temp_runningActions.insert (id);
        }

        /* difference between the running actions and the new actions to run */
        std::set<Id> diff;
        set_difference (runningActions_, temp_runningActions, diff);

        /* preempt all the actions that were running and should stop */
        for (auto const& id : diff) {
          id2primitive_[id]->stop();
        }

        /* execute the actions */
        for (auto const& id : temp_runningActions) {
          id2primitive_[id]->start();
        }

        runningActions_ = temp_runningActions;
      }

      /**
       * @brief Publishes the PAInfoMap
       *
       * Creates the correct PAInfoMap msg and publishes it.
       */
      void publishActionMap()
      {
        primitive_action_manager::PAInfoMap info_map;

        for (Id i = 0; i < id2name_.size(); i++) {
          primitive_action_manager::PAInfo info;
          info.name = id2name_[i];
          info.nr = i;
          info_map.map.push_back (info);
        }

        info_map.pa_id = robotName_;

        pa_map_pub_.publish (info_map);
      }
  };
}

#endif
