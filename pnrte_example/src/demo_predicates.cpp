#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <std_msgs/String.h>

#include <predicate_manager/predicate_manager.h>
#include <predicate_manager/dependencies.h>
#include <predicate_manager/prop_logic_predicate.h>
#include <predicate_manager/prop_logic_event.h>
#include <predicate_manager/prop_and.h>
#include <predicate_manager/prop_pv.h>
#include <predicate_manager/prop_not.h>



using namespace std;
using namespace ros;
using namespace predicate_manager;



class IsNearRefBoxPredicate : public Predicate
{
  public:
    IsNearRefBoxPredicate() : Predicate ("IsNearRefBox"),
      pose_subs_ (nh_.subscribe ("amcl_pose", 1, &IsNearRefBoxPredicate::poseCallback, this)),
      is_inside_area_ (false)
    {}
    void poseCallback (const geometry_msgs::PoseWithCovarianceStamped& msg)
    {
      bool aux = false;
      if (abs (msg.pose.pose.position.x - (7.96607)) < 1) {
        if (abs (msg.pose.pose.position.y - (-4.65841)) < 1) {
          aux = true;
        }
      }

      if (is_inside_area_ != aux) {
        is_inside_area_ = aux;
      }
      update();
    }

    void update()
    {
      setValue (is_inside_area_);
    }
  private:
    NodeHandle nh_;
    Subscriber pose_subs_;
    bool is_inside_area_;
};



class IsNearBedPredicate : public Predicate
{
  public:
    IsNearBedPredicate() : Predicate ("IsNearBed"),
      pose_subs_ (nh_.subscribe ("amcl_pose", 1, &IsNearBedPredicate::poseCallback, this)),
      is_inside_area_ (false)
    {}
    void poseCallback (const geometry_msgs::PoseWithCovarianceStamped& msg)
    {
      bool aux = false;
      if (abs (msg.pose.pose.position.x - (1.4378)) < 1) {
        if (abs (msg.pose.pose.position.y - (-2.8503)) < 1) {
          aux = true;
        }
      }

      if (is_inside_area_ != aux) {
        is_inside_area_ = aux;
      }
      update();
    }

    void update()
    {
      setValue (is_inside_area_);
    }
  private:
    NodeHandle nh_;
    Subscriber pose_subs_;
    bool is_inside_area_;
};



class IsNearTablePredicate : public Predicate
{
  public:
    IsNearTablePredicate() : Predicate ("IsNearTable"),
      pose_subs_ (nh_.subscribe ("amcl_pose", 1, &IsNearTablePredicate::poseCallback, this)),
      is_inside_area_ (false)
    {}
    void poseCallback (const geometry_msgs::PoseWithCovarianceStamped& msg)
    {
      bool aux = false;
      if (abs (msg.pose.pose.position.x - (6.35942)) < 1) {
        if (abs (msg.pose.pose.position.y - (-1.34058)) < 1) {
          aux = true;
        }
      }

      if (is_inside_area_ != aux) {
        is_inside_area_ = aux;
      }
      update();
    }

    void update()
    {
      setValue (is_inside_area_);
    }
  private:
    NodeHandle nh_;
    Subscriber pose_subs_;
    bool is_inside_area_;
};



class IsNearEntrancePredicate : public Predicate
{
  public:
    IsNearEntrancePredicate() : Predicate ("IsNearEntrance"),
      pose_subs_ (nh_.subscribe ("amcl_pose", 1, &IsNearEntrancePredicate::poseCallback, this)),
      is_inside_area_ (false)
    {}
    void poseCallback (const geometry_msgs::PoseWithCovarianceStamped& msg)
    {
      bool aux = false;
      if (abs (msg.pose.pose.position.x - (3.53307)) < 1) {
        if (abs (msg.pose.pose.position.y - (-1.28673)) < 1) {
          aux = true;
        }
      }

      if (is_inside_area_ != aux) {
        is_inside_area_ = aux;
      }
      update();
    }

    void update()
    {
      setValue (is_inside_area_);
    }
  private:
    NodeHandle nh_;
    Subscriber pose_subs_;
    bool is_inside_area_;
};



class IsInLRMPredicate : public Predicate
{
  public:
    IsInLRMPredicate() : Predicate ("IsNearLRMPosition"),
      pose_subs_ (nh_.subscribe ("amcl_pose", 1, &IsInLRMPredicate::poseCallback, this)),
      is_inside_area_ (false)
    {}
    void poseCallback (const geometry_msgs::PoseWithCovarianceStamped& msg)
    {
      bool aux = false;
      if (abs (msg.pose.pose.position.x - (-10.259)) < 1) {
        if (abs (msg.pose.pose.position.y - (-1.544)) < 1) {
          aux = true;
        }
      }

      if (is_inside_area_ != aux) {
        is_inside_area_ = aux;
      }
      update();
    }

    void update()
    {
      setValue (is_inside_area_);
    }
  private:
    NodeHandle nh_;
    Subscriber pose_subs_;
    bool is_inside_area_;
};



class IsNearElevatorPredicate : public Predicate
{
  public:
    IsNearElevatorPredicate() : Predicate ("IsNearElevator"),
      pose_subs_ (nh_.subscribe ("amcl_pose", 1, &IsNearElevatorPredicate::poseCallback, this)),
      is_inside_area_ (false)
    {}
    void poseCallback (const geometry_msgs::PoseWithCovarianceStamped& msg)
    {
      bool aux = false;
      if (abs (msg.pose.pose.position.x - (2.439)) < 1) {
        if (abs (msg.pose.pose.position.y - (-1.489)) < 1) {
          aux = true;
        }
      }

      if (is_inside_area_ != aux) {
        is_inside_area_ = aux;
      }
      update();
    }

    void update()
    {
      setValue (is_inside_area_);
    }
  private:
    NodeHandle nh_;
    Subscriber pose_subs_;
    bool is_inside_area_;
};



class userRequestPredicate : public Predicate
{
  public:
    userRequestPredicate() : Predicate ("userRequest"),
      sub_ (nh_.subscribe ("user_request", 1, &userRequestPredicate::callback, this)),
      is_true_ (false)
    {}
    void callback (const std_msgs::String& msg)
    {
      if (msg.data == "YES") {
        if (!is_true_) {
          is_true_ = true;
          update();
        }
      }
      else if (msg.data == "NO") {
        if (is_true_) {
          is_true_ = false;
          update();
        }
      }
    }

    void update()
    {
      setValue (is_true_);
    }
  private:
    NodeHandle nh_;
    Subscriber sub_;
    bool is_true_;
};



class IsNearCoffeeMachinePredicate : public Predicate
{
  public:
    IsNearCoffeeMachinePredicate() : Predicate ("IsNearCoffeeMachine"),
      pose_subs_ (nh_.subscribe ("amcl_pose", 1, &IsNearCoffeeMachinePredicate::poseCallback, this)),
      is_inside_area_ (false)
    {}
    void poseCallback (const geometry_msgs::PoseWithCovarianceStamped& msg)
    {
      bool aux = false;
      if (abs (msg.pose.pose.position.x - (-5.242)) < 1) {
        if (abs (msg.pose.pose.position.y - (11.084)) < 1) {
          aux = true;
        }
      }

      if (is_inside_area_ != aux) {
        is_inside_area_ = aux;
      }
      update();
    }

    void update()
    {
      setValue (is_inside_area_);
    }
  private:
    NodeHandle nh_;
    Subscriber pose_subs_;
    bool is_inside_area_;
};



class IsNearStairsPredicate : public Predicate
{
  public:
    IsNearStairsPredicate() : Predicate ("IsNearStairs"),
      pose_subs_ (nh_.subscribe ("amcl_pose", 1, &IsNearStairsPredicate::poseCallback, this)),
      is_inside_area_ (false)
    {}
    void poseCallback (const geometry_msgs::PoseWithCovarianceStamped& msg)
    {
      bool aux = false;
      if (abs (msg.pose.pose.position.x - (-0.901)) < 1) {
        if (abs (msg.pose.pose.position.y - (8.049)) < 1) {
          aux = true;
        }
      }

      if (is_inside_area_ != aux) {
        is_inside_area_ = aux;
      }
      update();
    }

    void update()
    {
      setValue (is_inside_area_);
    }
  private:
    NodeHandle nh_;
    Subscriber pose_subs_;
    bool is_inside_area_;
};



class IsInSoccerFieldPredicate : public Predicate
{
  public:
    IsInSoccerFieldPredicate() : Predicate ("IsInSoccerField"),
      pose_subs_ (nh_.subscribe ("amcl_pose", 1, &IsInSoccerFieldPredicate::poseCallback, this)),
      is_inside_area_ (false)
    {}
    void poseCallback (const geometry_msgs::PoseWithCovarianceStamped& msg)
    {
      bool aux = false;
      if (abs (msg.pose.pose.position.x - (-4.378)) < 1) {
        if (abs (msg.pose.pose.position.y - (-8.751)) < 1) {
          aux = true;
        }
      }

      if (is_inside_area_ != aux) {
        is_inside_area_ = aux;
      }
      update();
    }

    void update()
    {
      setValue (is_inside_area_);
    }
  private:
    NodeHandle nh_;
    Subscriber pose_subs_;
    bool is_inside_area_;
};



/**
 * An example of a Predicate that defines a condition over an active ROS topic.
 * (This Predicate isn't used in the accompanying MDP state representation)
 */
class IsMovingPredicate : public Predicate
{
  public:
    IsMovingPredicate() :
      Predicate ("IsMoving"),
      vel_subs_ (nh_.subscribe ("cmd_vel", 10, &IsMovingPredicate::velocityCallback, this)),
      is_moving_forward_ (false)
    {}

    void velocityCallback (const geometry_msgs::TwistConstPtr& msg)
    {
      bool val = msg->linear.x > 0.1;
      if (val != is_moving_forward_) {
        is_moving_forward_ = val;
        update();
      }
    }

    void update()
    {
      setValue (is_moving_forward_);
    }

  private:
    NodeHandle nh_;
    Subscriber vel_subs_;
    bool is_moving_forward_;
};



int main (int argc, char** argv)
{
  init (argc, argv, "predicate_manager");

  PredicateManager pm;

  IsMovingPredicate isMoving;
  IsInLRMPredicate isInLRM;
  IsNearElevatorPredicate isNearElevator;
  IsNearCoffeeMachinePredicate isNearCoffee;
  IsNearStairsPredicate isNearStairs;
  IsInSoccerFieldPredicate isInSoccerField;

  IsNearBedPredicate isNearBed;
  IsNearEntrancePredicate isNearEntrance;
  IsNearRefBoxPredicate isNearRefBox;
  IsNearTablePredicate isNearTable;

  userRequestPredicate userRequest;

  ///Registering predicates in the PM
  pm.addPredicate (isInLRM);
  pm.addPredicate (isNearElevator);
  pm.addPredicate (isMoving);
  pm.addPredicate (isNearCoffee);
  pm.addPredicate (isInSoccerField);
  pm.addPredicate (isNearStairs) ;
  pm.addPredicate (userRequest) ;

  pm.addPredicate (isNearBed);
  pm.addPredicate (isNearEntrance) ;
  pm.addPredicate (isNearRefBox) ;
  pm.addPredicate (isNearTable) ;

  ///Starting PM
  pm.spin();

  return 0;
}
