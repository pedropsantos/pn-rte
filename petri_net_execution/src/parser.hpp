#ifndef __PARSER__
#define __PARSER__

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/optional/optional.hpp>

#include <cstdlib>
#include <string>
#include <vector>

#include "ros/ros.h"



//* Parser
/**
 *  The Parser struct implements methods to parse and verfiy a pnml or xml file that describes a PetriNet.
 */
struct Parser {
  enum class PlaceType
  {
    ACTION = 0,
    PREDICATE,
    TASK,
    REGULAR
  };

  struct Color {
    typedef std::string Id;
    Id id_;
  };

  struct Node {
    typedef std::string Id;
    Id id_;
    std::vector<Node::Id> sources_;
    std::vector<Node::Id> targets_;
  };

  struct Place : Node {
    size_t capacity_;
    std::string name_;
    PlaceType type_;
  };

  struct Transition : Node {
    double rate_;
    int priori_;
    bool immediate_;
  };

  struct Arc : Node {
    std::map<Color::Id, size_t> cost_;
  };

  std::map<Arc::Id, Arc> arcs_;
  std::map<Place::Id, Place> places_;
  std::map<Transition::Id, Transition> transitions_;
  std::map<Color::Id, Color> colors_;

  std::map<Place::Id, std::map<Color::Id, size_t> > initial_marking_;

  ~Parser() {};

  /**
   * @brief Fill the source/target nodes to all places and transitions of the petrinet based on the arcs
   *
   * @return true, if eveything was correct and was able to fill the source/target
   *         false, otherwise
   */
  bool fillSourceTarget()
  {
    for (auto const& it : arcs_) {
      Arc a = it.second;
      if (a.sources_.size() != 1 || a.targets_.size() != 1) {
        ROS_INFO_STREAM ("ERROR ON ARC " << a.id_ << " SIZE OF THE VECTORS SOURCES AND TARGETS MUST BE EXACTLY 1");
        return false;
      }
      else {
        int sourcefound = 0;

        auto it_p = places_.find (a.sources_[0]);
        if (it_p != places_.end()) {
          it_p->second.targets_.push_back (a.id_);
          sourcefound = 1;
        }
        else {
          it_p = places_.find (a.targets_[0]);
          if (it_p != places_.end()) {
            it_p->second.sources_.push_back (a.id_);
          }
          else {
            ROS_ERROR_STREAM ("SOURCE OR TARGET OF THE ARC" << a.id_ << " IS NOT A PLACE");
            return false;
          }
        }

        if (sourcefound) {
          auto it_t = transitions_.find (a.targets_[0]);
          if (it_t != transitions_.end()) {
            it_t->second.sources_.push_back (a.id_);
          }
          else {
            ROS_ERROR_STREAM ("TARGET OF THE ARC" << a.id_ << " IS NOT A TRANSITION AS IT SHOULD BE");
            return false;
          }
        }
        else {
          auto it_t = transitions_.find (a.sources_[0]);
          if (it_t != transitions_.end()) {
            it_t->second.targets_.push_back (a.id_);
          }
          else {
            ROS_ERROR_STREAM ("SOURCE OF THE ARC" << a.id_ << " IS NOT A TRANSITION AS IT SHOULD BE");
            return false;
          }
        }
      }
    }
    return true;
  }

  /**
   * @brief Solve the capacities of the places
   *
   * Method used to solve the capacities of the petrinet places in order to have all places without capacities
   * for the petrinet execution. It solves the capacities by adding virtual places with the a number of tokens equal to
   * the capacity of the place and correctly connects them to the source and target transitions of the place in the
   * reverse order
   * @return true, if solved the capacities and everything was correct
   *         false, otherwise
   */
  bool solveCapacities()
  {
    std::map<Place::Id, Place> temp;
    for (auto const& p_it : places_) {
      if (p_it.second.capacity_ == 0) {
        continue;
      }
      else {
        /* Cria o lugar de capacidade cap_place, da o nome ao lugar, so existe para a execucao -> id e nome passam a
        ter o prefixo "cap." e a capacidade = 0 */
        Place cap_place;
        cap_place.id_ = "cap." + p_it.first;
        cap_place.capacity_ = 0;
        cap_place.name_ = "cap." + p_it.second.name_;
        cap_place.type_ = PlaceType::REGULAR;

        /* cria a initial marking correspondente ao lugar de capacidade, mete no initial marking um numero de tokens igual a
        capacidade do lugar que se esta a resolver */

        auto c_it = initial_marking_.find (p_it.first);
        int aux_counter = 0;
        if (c_it != initial_marking_.end()) {
          for (auto const& it : c_it->second) {
            aux_counter += it.second;
          }
        }

        aux_counter = p_it.second.capacity_ - aux_counter;

        bool insert_marking = false;

        /* Para cada source arc do lugar p */
        for (size_t i = 0; i < p_it.second.sources_.size(); ++i) {
          bool should_insert = true;
          auto const& a_it = arcs_.find (p_it.second.sources_[i]);  // encontra o arco na lista de arcos
          if (a_it != arcs_.end()) { // se arco existe
            auto const& t_it = transitions_.find (a_it->second.sources_[0]);  // encontra a transicao que e source do arco dado que o lugar e o target do arco
            if (t_it != transitions_.end()) { // se a transicao existe
              for (size_t j = 0; j < t_it->second.sources_.size(); ++j) { // percorre todos as sources da transicao
                if (arcs_[t_it->second.sources_[j]].sources_[0] == p_it.first) {
                  should_insert = false;
                  break;
                }
              }
              if (should_insert) {
                insert_marking = true;

                Arc tempArc;
                tempArc.id_ = "cap arc " + a_it->second.id_;
                tempArc.sources_.push_back (cap_place.id_);
                tempArc.targets_.push_back (t_it->first);
                tempArc.cost_[colors_.begin()->first] = 1;

                /* Adiciona o arco a lista de arcos */
                arcs_[tempArc.id_] = tempArc;

                /* adiciona o arco a "lista" de targets do novo lugar */
                cap_place.targets_.push_back (tempArc.id_);

                /* adiciona o arco a "lista" de sources da transicao */
                t_it->second.sources_.push_back (tempArc.id_);
              }
            }
            else {
              ROS_ERROR_STREAM ("ERROR TRANSFORMING CAPACITIES IN PLACES, TRANSITION " << a_it->second.sources_[0] << " DOES NOT EXIST");
              return false;
            }
          }
          else {
            ROS_ERROR_STREAM ("ERROR TRANSFORMING CAPACITIES IN PLACES, ARC " << p_it.second.sources_[i] << " DOES NOT EXIST");
            return false;
          }
        }

        for (size_t i = 0; i < p_it.second.targets_.size(); ++i) {
          bool should_insert = true;
          auto const& a_it = arcs_.find (p_it.second.targets_[i]);  // encontra o arco na lista de arcos
          if (a_it != arcs_.end()) { // se arco existe
            auto const& t_it = transitions_.find (a_it->second.targets_[0]);  // encontra a transicao que e target do arco dado que o lugar e source
            if (t_it != transitions_.end()) { // se a transicao existe
              for (size_t j = 0; j < t_it->second.targets_.size(); ++j) { // percorre todos as targets da transicao
                if (arcs_[t_it->second.targets_[j]].targets_[0] == p_it.first) { //se o lugar for source e target da transicao nao insere nada
                  should_insert = false;
                  break;
                }
              }
              if (should_insert) {
                insert_marking = true;

                Arc tempArc;
                tempArc.id_ = "cap arc " + a_it->second.id_;
                tempArc.sources_.push_back (t_it->first);
                tempArc.targets_.push_back (cap_place.id_);
                tempArc.cost_[colors_.begin()->first] = 1;

                /* Adiciona o arco a lista de arcos */
                arcs_[tempArc.id_] = tempArc;

                /* adiciona o arco a "lista" de targets do novo lugar */
                cap_place.sources_.push_back (tempArc.id_);

                /* adiciona o arco a "lista" de sources da transicao */
                t_it->second.targets_.push_back (tempArc.id_);
              }
            }
            else {
              ROS_ERROR_STREAM ("ERROR TRANSFORMING CAPACITIES IN PLACES, TRANSITION " << a_it->second.targets_[0] << " DOES NOT EXIST");
              return false;
            }
          }
          else {
            ROS_ERROR_STREAM ("ERROR TRANSFORMING CAPACITIES IN PLACES, ARC " << p_it.second.targets_[i] << " DOES NOT EXIST");
            return false;
          }
        }

        if (insert_marking) {
          temp[cap_place.id_] = cap_place;
          if (aux_counter < 0) {
            ROS_ERROR_STREAM ("ERROR SOLVING THE CAPACITIES OF THE PLACES");
            return false;
          }
          else if (aux_counter > 0) {
            std::map<Color::Id, size_t> m;
            m[colors_.begin()->first] = aux_counter;
            initial_marking_[cap_place.id_] = m;
          }
        }
      }
    }
    places_.insert (temp.begin(), temp.end());
    return true;
  }
  /**
   *
   */
  std::string q (const std::string& s)
  {
    return "\"" + s + "\"";
  }

  /**
   *
   */
  void printTree (const boost::property_tree::ptree& pt, int level)
  {
    const std::string sep (2 * level, ' ');
    for (auto const& v : pt) {
      ROS_INFO_STREAM (sep
                       << q (v.first) << " : " << v.second.data());
      printTree (v.second, level + 1);
    }
  }

  /**
   *
   */
  void printTree (const boost::property_tree::ptree& pt)
  {
    printTree (pt, 0);
  }

  /**
   * @brief Parse/"reads" the xml in order to extract one place
   *
   * @param pElement Element of the xml identified as place
   * @return true, if correctly parsed and inserted the place
   *         false, otherwise
   */
  bool extractPlace (const boost::property_tree::ptree& pt, bool tool)
  {
    Place p;
    std::string name, mark;

    p.id_ = pt.get_child ("<xmlattr>").get<std::string> ("id");
    if (tool) {
      name = pt.get_child ("name").get<std::string> ("text");
      p.capacity_ = pt.get_child ("toolspecific").get_child ("capacity").get<size_t> ("text");
      mark = pt.get_child ("initialMarking").get<std::string> ("text");
    }
    else {
      name = pt.get_child ("name").get<std::string> ("value");
      p.capacity_ = pt.get_child ("capacity").get<size_t> ("value");
      mark = pt.get_child ("initialMarking").get<std::string> ("value");
    }

    if (name.empty()) {
      ROS_ERROR_STREAM ("ERROR PARSING PLACE - name");
      return false;
    }

    std::vector<std::string> strs;
    boost::split (strs, name, boost::is_any_of ("."));
    if (strs.size() == 1) {
      p.name_ = name;
      p.type_ = PlaceType::REGULAR;
    }
    else {
      std::string prefix = strs[0];
      if (prefix == "action" || prefix == "a") {
        p.name_ = strs[1];
        p.type_ = PlaceType::ACTION;
      }
      else if (prefix == "predicate" || prefix == "p") {
        p.name_ = strs[1];
        p.type_ = PlaceType::PREDICATE;
      }
      else if (prefix == "task" || prefix == "t") {
        p.name_ = strs[1];
        p.type_ = PlaceType::TASK;
      }
      else {
        ROS_WARN_STREAM ("PREFIX NOT KNOWN, PLACE IS REGULAR");
        p.name_ = name;
        p.type_ = PlaceType::REGULAR;
      }
    }

    if (mark.empty()) {
      ROS_ERROR_STREAM ("ERROR PARSING PLACE - initialMarking");
      return false;
    }

    places_[p.id_] = p;
    std::vector<std::string> marking;
    boost::algorithm::split (marking, mark, boost::is_any_of (","));
    std::map<Color::Id, size_t> m;

    if (marking.size() == 1) { // PNLAB TOOL
      size_t value = boost::lexical_cast<size_t> (mark);
      if (value != 0) {
        m["Default"] = value;
      }
    }
    else {
      for (int i = 0; i < marking.size(); i = i + 2) {
        size_t value = boost::lexical_cast<size_t> (marking[i + 1]);
        if (value != 0) {
          m[marking[i]] = value;
        }
      }
    }
    if (!m.empty()) {
      initial_marking_[p.id_] = m;
    }
    return true;
  }

  /**
   * @brief Parse/"reads" the xml in order to extract one token
   *
   * @param pElement Element of the xml identified as token
   * @return true, if correctly parsed and inserted the token
   *         false, otherwise
   */
  bool extractToken (const boost::property_tree::ptree& pt, bool tool)
  {
    if (!tool) {
      if (pt.get_child ("<xmlattr>").get<bool> ("enabled")) {
        Color c;
        c.id_ = pt.get_child ("<xmlattr>").get<std::string> ("id");
        colors_[c.id_] = c;
        return true;
      }
    }
    return false;
  }

  /**
   * @brief Parse/"reads" the xml in order to extract one transition
   *
   * @param pElement Element of the xml identified as transition
   * @return true, if correctly parsed and inserted the transition
   *         false, otherwise
   */
  bool extractTransition (const boost::property_tree::ptree& pt, bool tool)
  {
    Transition t;

    t.id_ = pt.get_child ("<xmlattr>").get<std::string> ("id");
    if (tool) {
      for (auto const& v : pt) {
        if (v.first == "toolspecific") {
          if (v.second.get_child ("<xmlattr>").get<std::string> ("tool") == "PNLab") {
            t.rate_ = v.second.get_child ("rate").get<double> ("text");
            t.priori_ = v.second.get_child ("priority").get<int> ("text");
            std::string aux = v.second.get_child ("type").get<std::string> ("text");
            if (aux == "immediate") {
              t.immediate_ = true;
            }
            else {
              t.immediate_ = false;
            }
            transitions_[t.id_] = t;
            return true;
          }
        }
      }
    }
    else {
      t.rate_ = pt.get_child ("rate").get<double> ("value");
      t.priori_ = pt.get_child ("priority").get<int> ("value");
      std::string aux = pt.get_child ("timed").get<std::string> ("value");
      if (aux == "0" || aux == "false") {
        t.immediate_ = true;
      }
      else {
        t.immediate_ = false;
      }
      transitions_[t.id_] = t;
      return true;
    }
    return false;
  }

  /**
   * @brief Parse/"reads" the xml in order to extract one arc
   *
   * @param pElement Element of the xml identified as arc
   * @return true, if correctly parsed and inserted the arc
   *         false, otherwise
   */
  bool extractArc (const boost::property_tree::ptree& pt, bool tool)
  {
    Arc a;

    a.id_ = pt.get_child ("<xmlattr>").get<std::string> ("id");
    a.sources_.push_back (pt.get_child ("<xmlattr>").get<std::string> ("source"));
    a.targets_.push_back (pt.get_child ("<xmlattr>").get<std::string> ("target"));

    std::string cost;
    if (tool) {
      cost = pt.get_child ("inscription").get<std::string> ("text");
    }
    else {
      cost = pt.get_child ("inscription").get<std::string> ("value");
    }

    if (cost.empty()) {
      ROS_ERROR_STREAM ("ERROR PARSING ARC - Cost/Marking");
      return false;
    }

    std::vector<std::string> cost_aux;
    boost::algorithm::split (cost_aux, cost, boost::is_any_of (","));
    if (cost_aux.size() == 1) { // PNLAB TOOL
      a.cost_["Default"] = boost::lexical_cast<size_t> (cost);
    }
    else {
      for (int i = 0; i < cost_aux.size(); i = i + 2) {
        a.cost_[cost_aux[i]] = boost::lexical_cast<size_t> (cost_aux[i + 1]);
      }
    }
    arcs_[a.id_] = a;
    return true;
  }

  /**
   * @brief Constructor
   *
   * Constructor of the Parser class, responsible to parse the petrinet file
   * @param filename of the petrinet file (pnml or xml)
   */
  Parser (const std::string& filename)
  {
    try {
      boost::property_tree::ptree pt;
      boost::property_tree::xml_parser::read_xml (filename, pt);

      bool tool;
      size_t cnTok = 0, cnPla = 0, cnTrn = 0, cnArc = 0;

      boost::optional<boost::property_tree::ptree& > child = pt.get_child ("pnml").get_child_optional ("<xmlattr>");
      if (!child) {
        ROS_INFO_STREAM ("file from PIPE");
        pt = pt.get_child ("pnml").get_child ("net");
        tool = 0;
      }
      else {
        ROS_INFO_STREAM ("file from PNLab");
        pt = pt.get_child ("pnml").get_child ("net").get_child ("page");

        tool = 1;
      }

      for (auto const& v : pt) {
        std::string value = v.first;
        if (value == "place") {
          if (!extractPlace (v.second, tool)) {
            ros::shutdown();
          }
          cnPla++;
        }
        else if (value == "token") {
          if (!extractToken (v.second, tool)) {
            ros::shutdown();
          }
          cnTok++;
        }
        else if (value == "transition") {
          if (!extractTransition (v.second, tool)) {
            ros::shutdown();
          }
          cnTrn++;
        }
        else if (value == "arc") {
          if (!extractArc (v.second, tool)) {
            ros::shutdown();
          }
          cnArc++;
        }
        else if (value == "name" || value == "<xmlattr>" || value == "toolspecific") {
          //ROS_INFO_STREAM(value <<" - do not need to parse this info");
          continue;
        }
        else {
          ROS_WARN_STREAM ("Unknown : " << value);
        }
      }
      if (cnTok == 0) { //PNLAB TOOL
        Color c;
        c.id_ = "Default";
        colors_[c.id_] = c;
        cnTok++;
      }
      ROS_INFO_STREAM (/*"Tokens " << cnTok << */" Places " << cnPla << " Transition " << cnTrn << " Arc " << cnArc);
    }
    catch (std::exception& e) {
      ROS_FATAL_STREAM ("ERROR: " << e.what());
      ros::shutdown();
    }

    if (!fillSourceTarget()) {
      ros::shutdown();
    }

    if (!solveCapacities()) {
      ros::shutdown();
    }

    if (initial_marking_.empty()) {
      ROS_ERROR_STREAM ("Initial Marking is empty, ignore this message if this was your plan or kill the process and correct the pnml file");
    }

    printAllData();
  }


  /**
   * @brief prints all the parsed xml data
   */
  void printAllData()
  {
    /* for (auto const& it : colors_) {
      printToken (it.second);
      }*/

    for (auto const& it : arcs_) {
      printArc (it.second);
    }

    for (auto const& it : places_) {
      printPlace (it.second);
    }

    for (auto const& it : transitions_) {
      printTransition (it.second);
    }
  }

  void printToken (const Color& c)
  {
    ROS_INFO_STREAM ("TOKEN COLOR: " << c.id_);
  }

  /**
   * @brief prints the parsed data of a place
   */
  void printPlace (const Place& p)
  {
    ROS_INFO_STREAM ("PLACE: " << p.id_);
    ROS_INFO_STREAM ("\tNAME: " << p.name_);
    if (p.capacity_ == 0) {
      ROS_INFO_STREAM ("\tCAPACITY: INF");
    }
    else {
      ROS_INFO_STREAM ("\tCAPACITY: " << p.capacity_);
    }

    ROS_INFO_STREAM ("\tMARKING: ");
    auto iter = initial_marking_.find (p.id_);
    if (iter != initial_marking_.end()) {
      int i = 0;
      for (auto const& it : iter->second) {
        if (i == 0) {
          ROS_INFO_STREAM ("\t\tTOKEN COLOR: ");
          i = 1;
        }
        ROS_INFO_STREAM ("\t\t" << it.first << " -- Value: " << it.second);
      }
    }
    printSourceTarget (p);
  }

  /**
   * @brief prints the parsed data of an arc
   */
  void printArc (const Arc& a)
  {
    ROS_INFO_STREAM ("ARC: " << a.id_);
    int i = 0;
    for (auto const& it : a.cost_) {
      if (i == 0) {
        ROS_INFO_STREAM ("\tTOKEN COLOR: ");
        i = 1;
      }
      ROS_INFO_STREAM ("\t\t" << it.first << " -- Value: " << it.second);
    }
    printSourceTarget (a);
  }

  /**
   * @brief prints the parsed data of a transition
   */
  void printTransition (const Transition& t)
  {
    ROS_INFO_STREAM ("TRANSITION: " << t.id_);
    if (t.immediate_) {
      ROS_INFO_STREAM ("\tIMMEDIATE");
      //  ROS_INFO_STREAM ("\t\tPRIORITY: " << t.priori);
    }
    else {
      ROS_INFO_STREAM ("\tSTOCHASTIC");
      //  ROS_INFO_STREAM ("\t\tRATE: " << t.rate);
    }
    printSourceTarget (t);
  }

  /**
   * @brief prints the source and target lists of a node
   */
  void printSourceTarget (const Node& n)
  {
    ROS_INFO_STREAM ("\tSOURCES: ");
    for (size_t it = 0; it < n.sources_.size(); ++it) {
      ROS_INFO_STREAM ("\t\t" << n.sources_[it]);
    }
    ROS_INFO_STREAM ("\tTARGETS: ");
    for (size_t it = 0; it < n.targets_.size(); ++it) {
      ROS_INFO_STREAM ("\t\t" << n.targets_[it]);
    }
  }
};

#endif
