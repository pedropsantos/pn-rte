#ifndef __PETRINET_DEPENDENT__
#define __PETRINET_DEPENDENT__

#include <boost/foreach.hpp>
#include <boost/generator_iterator.hpp>
#include <boost/random.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>

#include <algorithm>

#include <cstdlib>
#include <ctime>
#include <string>
#include <vector>

#include <predicate_manager/PredicateUpdate.h>

#include "ros/ros.h"
#include "petrinetstructure.hpp"



class PetriNetExecutor
{
    std::shared_ptr<PetriNetStructure> petrinet_state_;

    ros::NodeHandle nh_;

    ros::Subscriber pred_updates_sub_;
    ros::Subscriber pred_map_sub_;
    ros::Subscriber pa_map_sub_;

    ros::Publisher pa_update_pub_;

    boost::mt19937 gen_;

    std::map<Place::Id, Cost> curr_marking_;

    bool first_update_;
    bool changed_;


    /**
     * @brief Given the current state of the PetriNet, currMarking, computes the Transitions that may fire
     * @return The set of active/enable transitions
     */
    std::set<Transition::Id> activeTransition (const std::map<Place::Id, Cost>& marking)
    {
      std::set<Transition::Id> temp;
      std::map<Place::Id, Cost> temp_mark = marking;
      bool active;
      for (auto const& t_it : petrinet_state_->backwards_matrix_) {
        active = true;
        for (auto const& p_it : t_it.second) {
          if (!active) {
            break;
          }
          for (auto const& c_it : p_it.second) {
            if (petrinet_state_->backwards_matrix_[t_it.first][p_it.first][c_it.first] > temp_mark[p_it.first][c_it.first]) {
              active = false;
              break;
            }
          }
        }
        if (active) {
          temp.insert (t_it.first);
        }
      }
      return temp;
    }



    /**
     * @brief Insert the set of "active transitions" into the set of "to visited transitions"
     */
    std::set<Transition::Id> insertActiveTransitions (const std::set<Transition::Id>& active_transitions, const std::set<Transition::Id>& visited_transitions)
    {
      std::set<Transition::Id> temp = visited_transitions;
      for (auto transition_it : active_transitions) {
        temp.insert (transition_it);
      }
      return temp;
    }



    /**
     * @brief randomly choose a Transition to Fire from the set of activeTransition_
     * @return The id of the chosen transition (aka the chosen one aka the special one)
     */
    Transition::Id chooseTransition (const std::set<Transition::Id>& active_transitions)
    {
      static unsigned int seed = 0;
      gen_.seed ( (++seed) + time (NULL));

      boost::uniform_int<> dist (0, active_transitions.size() - 1);
      boost::variate_generator< boost::mt19937, boost::uniform_int<> > func (gen_, dist);

      auto t_it = active_transitions.begin();
      advance (t_it, func());
      return *t_it;
    }



    /**
     * @brief Fires the transition and computes the new state of the PetriNet, currMarking
     * @param id the id of the transition to fire
     */
    std::map<Place::Id, Cost> fireTransition (Transition::Id id, const std::map<Place::Id, Cost>& marking)
    {
      std::map<Place::Id, Cost> temp;
      std::map<Place::Id, Cost> mark = marking;
      auto t_it = petrinet_state_->backwards_matrix_.find (id);
      if (t_it != petrinet_state_->backwards_matrix_.end()) {
        for (auto const& p_it : t_it->second) {
          for (auto const& c_it : p_it.second) {
            int value = mark[p_it.first][c_it.first] - petrinet_state_->backwards_matrix_[t_it->first][p_it.first][c_it.first] + petrinet_state_->forwards_matrix_[t_it->first][p_it.first][c_it.first];
            if (value > 0) {
              temp[p_it.first][c_it.first] = value;
            }
            else if (value < 0) {
              ROS_ERROR_STREAM ("petrinet_dependet_fireTransition method error: VALUE CAN'T BE NEGATIVE");
              ros::shutdown();
            }
          }
        }
        return temp;
      }
    }



    /**
     * @brief check if the petrinet execution is in a live-lock state
     * @return true, if the petrinet is in a live-lock state
     *         false otherwise
     */
    bool isInLiveLock (const std::set<Transition::Id>& visited_transitions, const std::set<Transition::Id>& active_transitions, const std::set<std::map<Place::Id, Cost> >& visited, const std::map<Place::Id, Cost>& marking)
    {
      if (visited.count (marking) == 1) {
        if (includes (visited_transitions.begin(), visited_transitions.end(), active_transitions.begin(), active_transitions.end())) {
          return true;
        }
      }
      return false;
    }



    /**
     * @brief computes the petrinet state, currMarking, while exist active transitions
     */
    void executeNet()
    {
      std::set<std::map<Place::Id, Cost> > visited;
      std::set<Transition::Id> visitedTransitions;
      std::set<Transition::Id> activeTransitions;
      std::map<Place::Id, Cost> auxMarking = curr_marking_;

      visited.clear();
      visitedTransitions.clear();
      activeTransitions.clear();

      activeTransitions = activeTransition (auxMarking);
      while (!activeTransitions.empty()) {
        if (isInLiveLock (visitedTransitions, activeTransitions, visited, auxMarking)) {
          // ROS_WARN_STREAM ("LiveLock DETECTED - please verify your petri net and/or the value of the predicates, the action(s) running will not be updated");
          return;
        }
        visited.insert (auxMarking);

        Transition::Id toFire = chooseTransition (activeTransitions);
        ROS_INFO_STREAM ("Executing petri net " << getName() << " \n going to fire transition: " << toFire);

        auxMarking = fireTransition (toFire, auxMarking);
        visitedTransitions.insert (toFire);

        activeTransitions = activeTransition (auxMarking);
      }
      if (auxMarking == curr_marking_) {
        changed_ = false;
      }
      else {
        changed_ = true;
        curr_marking_ = auxMarking;
      }
    }

  public:

    /**
     * @brief constructor PetriNetDependent
     * @param filename The filename of the xml file that describes the PetriNet
     */
    PetriNetExecutor (std::shared_ptr<PetriNetStructure> const& state) : petrinet_state_ (state),
      curr_marking_ (petrinet_state_->init_marking_),
      first_update_ (true),
      changed_ (false)
    {
      //printMarking();
    };
    ~PetriNetExecutor() {};

    /**
     * @brief Prints Marking
     */
    void printMarking (const std::map<Place::Id, Cost>& marking)
    {
      ROS_INFO_STREAM ("Marking:");
      for (auto const& iter : marking) {
        ROS_INFO_STREAM ("\tPLACE: " << petrinet_state_->placeId2Name (iter.first));
        for (auto const& it : iter.second) {
          ROS_INFO_STREAM ("\t\tCOLOR: " << petrinet_state_->colorsId2Id (it.first) << " Value: " << it.second);
        }
      }
    }

    std::string getName()
    {
      return petrinet_state_->getName();
    }
    /**
     * @brief Prints the current Marking
     */
    void printMarking()
    {
      ROS_INFO_STREAM ("Marking:");
      for (auto const& iter : curr_marking_) {
        ROS_INFO_STREAM ("\tPLACE: " << petrinet_state_->placeId2Name (iter.first));
        for (auto const& it : iter.second) {
          ROS_INFO_STREAM ("\t\tCOLOR: " << petrinet_state_->colorsId2Id (it.first) << " Value: " << it.second);
        }
      }
    }

    // void resetInitialMarking(){
    //   currMarking_ = petrinet_state_->initMarking_;
    // }

    /**
     * @brief
     */
    void updatePredicatesAndExecute (const predicate_manager::PredicateUpdateConstPtr& msg)
    {
      if (petrinet_state_->pm_id2pn_id_.empty()) {
        ROS_WARN_STREAM ("PETRINET_DEPENDENT - updatePredicatesAndExecute: Got an update before the predicate map, the update was discarded");
        ROS_WARN_STREAM ("Please publish the predicate manager map!");
        return;
      }

      if (petrinet_state_->pa_id2pn_id_.empty()) {
        ROS_WARN_STREAM ("PETRINET_DEPENDENT - updatePredicatesAndExecute: Got an update before the primitive action manager map, the update was discarded");
        ROS_WARN_STREAM ("Please publish the primitive action manager map!");
        return;
      }

      if (first_update_) {
        for (auto const& pn_p_id : petrinet_state_->predicates_) {
          auto pm_p_id = std::find (msg->true_predicates.begin(), msg->true_predicates.end(), petrinet_state_->pn_id2pm_id_[pn_p_id]);
          if (pm_p_id == msg->true_predicates.end()) {
            if (petrinet_state_->placeId2Name (pn_p_id).substr (0, 4) == "NOT_") {
              Cost temp;
              temp[msg->pm_id] = 1;
              curr_marking_[pn_p_id] = temp;
            }
            else {
              Cost temp = curr_marking_[pn_p_id];
              temp.erase (msg->pm_id);
              if (temp.empty()) {
                curr_marking_.erase (pn_p_id);
              }
              else {
                curr_marking_[pn_p_id] = temp;
              }
            }
          }
          else {
            if (petrinet_state_->placeId2Name (pn_p_id).substr (0, 4) == "NOT_") {
              Cost temp = curr_marking_[pn_p_id];
              temp.erase (msg->pm_id);
              if (temp.empty()) {
                curr_marking_.erase (pn_p_id);
              }
              else {
                curr_marking_[pn_p_id] = temp;
              }
            }
            else {
              Cost temp;
              temp[msg->pm_id] = 1;
              curr_marking_[pn_p_id] = temp;
            }
          }
        }
        first_update_ = false;
      }
      else {
        // Verifica os predicados que se tornaram true
        if (!msg->rising_predicates.empty()) {
          for (auto const& id : msg->rising_predicates) {
            auto range = petrinet_state_->pm_id2pn_id_.equal_range (id);
            for (auto pn_p_it = range.first; pn_p_it != range.second; ++pn_p_it) {
              if (petrinet_state_->placeId2Name (pn_p_it->second).substr (0, 4) == "NOT_") {
                Cost temp = curr_marking_[pn_p_it->second];
                temp.erase (msg->pm_id);
                if (temp.empty()) {
                  curr_marking_.erase (pn_p_it->second);
                }
                else {
                  curr_marking_[pn_p_it->second] = temp;
                }
              }
              else {
                Cost temp;
                temp[msg->pm_id] = 1;
                curr_marking_[pn_p_it->second] = temp;
              }
            }
          }
        }

        // Verifica os predicados que se to// rnaram false
        if (!msg->falling_predicates.empty()) {
          for (auto const& id : msg->falling_predicates) {
            auto range = petrinet_state_->pm_id2pn_id_.equal_range (id);
            for (auto pn_p_it = range.first; pn_p_it != range.second; ++pn_p_it) {
              if (petrinet_state_->placeId2Name (pn_p_it->second).substr (0, 4) == "NOT_") {
                Cost temp;
                temp[msg->pm_id] = 1;
                curr_marking_[pn_p_it->second] = temp;
              }
              else {
                Cost temp = curr_marking_[pn_p_it->second];
                temp.erase (msg->pm_id);
                if (temp.empty()) {
                  curr_marking_.erase (pn_p_it->second);
                }
                else {
                  curr_marking_[pn_p_it->second] = temp;
                }
              }
            }
          }
        }
      }
      executeNet();
    }

    bool stateChanged()
    {
      return changed_;
    }

    std::set<size_t> getActions2Execute()
    {
      std::set<size_t> aux;
      for (auto const& iter : curr_marking_) {
        bool is_in = petrinet_state_->actions_.find (iter.first) != petrinet_state_->actions_.end();
        if (is_in) {
          aux.insert (petrinet_state_->pn_id2pa_id_[iter.first]);
        }
      }
      return aux;
    }

    std::set<std::string> getTasks2Execute()
    {
      std::set<std::string> aux;
      for (auto const& it_place : curr_marking_) {
        const bool is_in = petrinet_state_->tasks_.find (it_place.first) != petrinet_state_->tasks_.end();
        if (is_in) {
          aux.insert (petrinet_state_->place_id2name_[it_place.first]);
        }
      }
      return aux;
    }

    /**
     * @brief Prints the set of active transitions
     */
    void printActiveTransitions (const std::set<Transition::Id>& active_transitions)
    {
      ROS_INFO_STREAM ("Active transitions: ");
      for (auto const& iter : active_transitions) {
        ROS_INFO_STREAM ("\t" << petrinet_state_->transitionId2Id (iter));
      }
    }
};

#endif
