#ifndef __PN_MANAGER__
#define __PN_MANAGER__

#include "petrinetstructure.hpp"
#include "petrinetexecutor.hpp"
#include <boost/filesystem.hpp>

#include <predicate_manager/predicate_manager.h>
#include <predicate_manager/PredicateInfoMap.h>
#include <predicate_manager/PredicateUpdate.h>
#include <primitive_action_manager/PAInfo.h>
#include <primitive_action_manager/PAInfoMap.h>
#include <primitive_action_manager/PAUpdate.h>



class Manager
{
    ros::NodeHandle nh_;

    std::string pn_name_;
    std::string wd_;
    std::string robot_name_;

    std::map<std::string, std::shared_ptr<PetriNetStructure> > petrinets_;
    std::map<std::string, std::shared_ptr<PetriNetExecutor> > nets_;
    std::map<std::string, std::shared_ptr<PetriNetExecutor> > aux_nets_;

    //std::unique_ptr<PetriNetDependent> top_net_;

    ros::Subscriber pred_updates_sub_;
    ros::Subscriber pred_map_sub_;
    ros::Subscriber pa_map_sub_;

    ros::Publisher pa_update_pub_;

    bool has_actions_;
    bool has_predicates_;

    predicate_manager::PredicateUpdateConstPtr pm_msg_;

    std::set<size_t> actions_;

  public:
    Manager() : has_actions_ (false),
      has_predicates_ (false),
      pred_map_sub_ (nh_.subscribe ("predicate_maps", 1, &Manager::predicateMapCallback, this)),
      pred_updates_sub_ (nh_.subscribe ("predicate_updates", 1, &Manager::predicateUpdateCallback, this)),
      pa_map_sub_ (nh_.subscribe ("action_map", 1, &Manager::pamanagerMapCallback, this)),
      pa_update_pub_ (nh_.advertise<primitive_action_manager::PAUpdate> ("action_update", 1, true))
    {
      if (nh_.getParam ("robot_name", robot_name_)) {
        addNet();
      }
      else {
        ROS_FATAL_STREAM ("Please set the name of your robot on the launch file");
        ros::shutdown();
      }
    };

    ~Manager() {};

    void addNet()
    {
      if (nh_.getParam ("pn_filename", pn_name_)) {
        if (nh_.getParam ("pn_wd", wd_)) {
          if (boost::filesystem::exists (wd_)) {
            std::string filename;
            filename = wd_ + pn_name_ + ".xml";
            if (!boost::filesystem::exists (filename)) {
              filename = wd_ + pn_name_ + ".pnml";
              if (!boost::filesystem::exists (filename)) {
                ROS_FATAL_STREAM ("Can't find file " << filename);
                ros::shutdown();
              }
            }
            ROS_INFO_STREAM (filename);
            petrinets_[pn_name_] = std::make_shared<PetriNetStructure> (filename);

            for (const auto& it : petrinets_[pn_name_]->tasks_) {
              if (petrinets_.count (petrinets_[pn_name_]->place_id2name_[it]) == 0) {
                addNet (petrinets_[pn_name_]->place_id2name_[it]);
              }
            }
          }
          else {
            ROS_FATAL_STREAM ("The working directory set on the launch file doesn't exist");
            ros::shutdown();
          }
        }
        else {
          ROS_FATAL_STREAM ("Please set the working directory of your petrinet on the launch file");
          ros::shutdown();
        }
      }
      else {
        ROS_FATAL_STREAM ("Please set the filename of your petrinet on the launch file");
        ros::shutdown();
      }
    }

    void addNet (const std::string& name)
    {
      std::string filename = wd_ + name + ".xml";
      if (!boost::filesystem::exists (filename)) {
        filename = wd_ + name + ".pnml";
        if (!boost::filesystem::exists (filename)) {
          ROS_FATAL_STREAM ("Can't find file " << filename);
          ros::shutdown();
        }
      }
      petrinets_[name] = std::make_shared<PetriNetStructure> (filename);

      for (const auto& it : petrinets_[name]->tasks_) {
        auto pn_it = petrinets_.find (petrinets_[name]->place_id2name_[it]);
        if (pn_it == petrinets_.end()) {
          addNet (petrinets_[name]->place_id2name_[it]);
        }

        // if(petrinets_.count(petrinets_[name]->place_id2name_[it])==0){
        //   addNet(petrinets_[name]->place_id2name_[it]);
        // }
      }
    }

    void executeNet (const predicate_manager::PredicateUpdateConstPtr& msg)
    {
      /* clear the set of actions that were executing */
      actions_.clear();
      aux_nets_.clear();

      auto pn = nets_.find (pn_name_);
      if (pn == nets_.end()) {
        nets_.clear();
        nets_[pn_name_] = std::make_shared<PetriNetExecutor> (petrinets_[pn_name_]);
      }
      aux_nets_[pn_name_] = nets_[pn_name_];
      aux_nets_[pn_name_]->updatePredicatesAndExecute (msg);

      std::set<size_t> aux_actions = aux_nets_[pn_name_]->getActions2Execute();
      actions_.insert (aux_actions.begin(), aux_actions.end());

      std::set<std::string> aux_tasks = aux_nets_[pn_name_]->getTasks2Execute();

      for (const auto& task_name : aux_tasks) {
        executeNet (task_name, msg);
      }

      nets_ = aux_nets_;
      aux_nets_.clear();

      publishCortexUpdate();
    }

    void executeNet (const std::string& name, const predicate_manager::PredicateUpdateConstPtr& msg)
    {
      auto pn = nets_.find (name);
      if (pn == nets_.end()) {
        auto it = petrinets_.find (name);
        if (it != petrinets_.end()) {
          aux_nets_[name] = std::make_shared<PetriNetExecutor> (it->second);
          aux_nets_[name]->updatePredicatesAndExecute (msg);
          std::set<size_t> aux_actions = aux_nets_[name]->getActions2Execute();
          actions_.insert (aux_actions.begin(), aux_actions.end());

          std::set<std::string> aux_tasks = aux_nets_[name]->getTasks2Execute();

          for (auto task_name : aux_tasks) {
            executeNet (task_name, msg);
          }
        }
        else {
          ROS_FATAL_STREAM ("ERROR petri net " << name << " was not loaded!");
         ros::shutdown();
        }
      }
      else {
        auto aux_pn = aux_nets_.find (name);
        if (aux_pn != aux_nets_.end()) {
          ROS_WARN_STREAM ("Possible cycle detected not going to execute again the petrinet " << name);
          return;
        }
        aux_nets_[name] = nets_[name];
        aux_nets_[name]->updatePredicatesAndExecute (msg);
        std::set<size_t> aux_actions = aux_nets_[name]->getActions2Execute();
        actions_.insert (aux_actions.begin(), aux_actions.end());
        std::set<std::string> aux_tasks = aux_nets_[name]->getTasks2Execute();
        for (auto task_name : aux_tasks) {
          executeNet (task_name, msg);
        }
      }
    }

    // void createTopNet(){
    //   nets_.clear();
    //   nets_[pn_name_] = std::make_shared<PetriNetDependent> (petrinets_[pn_name_]);

    //   //top_net_.reset();
    //   //top_net_.reset(new PetriNetDependent(petrinets_[pn_name_]));
    // }

    void pamanagerMapCallback (const primitive_action_manager::PAInfoMapConstPtr& msg)
    {
      std::map<std::string, uint32_t> aux_map;
      bool load = true;

      for (const auto& it : msg->map) {
        aux_map[it.name] = it.nr;
      }

      if (aux_map.empty()) {
        ROS_WARN_STREAM ("Did not load the primitive action manager map, empty map");
        has_actions_ = false;
        return;
      }

      for (auto it : petrinets_) {
        bool aux = it.second->paMapCallback (aux_map);
        load = load & aux;
      }

      if (!load) {
        ROS_WARN_STREAM ("Did not load the primitive action manager map, please correct the issues and publish the correct map");
        has_actions_ = false;
        return;
      }

      has_actions_ = true;
      if (has_predicates_) {
        executeNet (pm_msg_);
      }
    }

    void predicateMapCallback (const predicate_manager::PredicateInfoMapConstPtr& msg)
    {
      std::map<std::string, uint32_t> aux_map;
      bool load = true;

      for (const auto& it : msg->map) {
        aux_map[it.name] = it.nr;
      }

      if (aux_map.empty()) {
        ROS_WARN_STREAM ("Did not load the predicate manager map, empty map");
        has_predicates_ = false;
        return;
      }

      for (auto it : petrinets_) {
        bool aux = it.second->pmMapCallback (aux_map);
        load = load & aux;
      }

      if (!load) {
        ROS_WARN_STREAM ("Did not load the predicate manager map, please correct the issues and publish the correct map");
        has_predicates_ = false;
        return;
      }

      has_predicates_ = true;
      if (has_actions_) {
        executeNet (pm_msg_);
      }
    }

    void predicateUpdateCallback (const predicate_manager::PredicateUpdateConstPtr& msg)
    {
      pm_msg_ = msg;
      if (!has_predicates_) {
        ROS_WARN_STREAM ("PNMANAGER-predicateUpdateCallback: Got an update before the predicate map, the update was discarded");
        ROS_WARN_STREAM ("Please publish the predicate manager map!");
        return;
      }

      if (!has_actions_) {
        ROS_WARN_STREAM ("PNMANAGER-predicateUpdateCallback: Got an update before the primitive action manager map, the update was discarded");
        ROS_WARN_STREAM ("Please publish the primitive action manager map!");
        return;
      }
      executeNet (pm_msg_);
    }

    void publishCortexUpdate()
    {
      if (!has_actions_) {
        ROS_WARN_STREAM ("Can not send an action update without the primitive action manager map");
        ROS_WARN_STREAM ("Please publish the primitive action manager map");
        return;
      }

      if (!has_predicates_) {
        ROS_WARN_STREAM ("Can not send and action update without the predicate manager map");
        ROS_WARN_STREAM ("Please publish the predicate manager map!");
        return;
      }

      primitive_action_manager::PAUpdate update;
      update.robot_id = robot_name_;
      std::copy (actions_.begin(), actions_.end(), std::back_inserter (update.execute));
      pa_update_pub_.publish (update);
      actions_.clear();
    }
};
#endif
