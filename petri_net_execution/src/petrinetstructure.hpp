#ifndef __PETRINET_INDEPENDENT__
#define __PETRINET_INDEPENDENT__

#include <cstdlib>
#include <string>
#include <vector>

#include "ros/ros.h"
#include "parser.hpp"



struct Place {
  typedef size_t Id;
  Id id_;
};



struct Transition {
  typedef size_t Id;
  Id id_;
};



struct Color {
  typedef size_t Id;
  Id id_;
};



typedef std::map<Color::Id, size_t> Cost;



class PetriNetStructure
{
  protected:

    Parser source_;
    std::string filename_;

    /**
     * @brief Extract all places from the xml parsed data to the PetriNet Execution format
     */
    void extractPlaces()
    {
      place_string2id_.clear();
      num_places_ = source_.places_.size();
      place_id2string_.resize (num_places_);
      place_id2name_.resize (num_places_);

      size_t id = 0;
      for (auto const& it : source_.places_) {
        place_string2id_[it.first] = id;
        place_id2string_[id] = it.first;

        place_id2name_[id] = it.second.name_;

        if (it.second.type_ == Parser::PlaceType::ACTION) {
          actions_.insert (id);
        }
        else if (it.second.type_ == Parser::PlaceType::PREDICATE) {
          predicates_.insert (id);
        }
        else if (it.second.type_ == Parser::PlaceType::TASK) {
          tasks_.insert (id);
        }
        else {
          regular_.insert (id);
        }
        id++;
      }
      if (num_places_ != id) {
        ROS_ERROR_STREAM (" ERROR EXTRACTED A WRONG NUMBER OF PLACES, extracted " << id << " instead of " << num_places_);
        ros::shutdown();
      }
    }

    /**
     * @brief Extract all transitions from the xml parsed data to the PetriNet Execution format
     */
    void extractTransitions()
    {
      transition_string2id_.clear();
      num_transitions_ = source_.transitions_.size();
      transition_id2string_.resize (num_transitions_);
      size_t id = 0;
      for (auto const& it : source_.transitions_) {
        transition_string2id_[it.first] = id;
        transition_id2string_[id] = it.first;
        id++;
      }
      if (num_transitions_ != id) {
        ROS_ERROR_STREAM (" ERROR EXTRACTED A WRONG NUMBER OF TRANSITIONS, extracted " << id << " instead of " << num_transitions_);
        ros::shutdown();
      }
    }

    /**
     * @brief Extract all tokens/colors from the xml parsed data to the PetriNet Execution format
     */
    void extractColors()
    {
      colors_string2id_.clear();
      num_colors_ = source_.colors_.size();
      colors_id2string_.resize (num_colors_);
      size_t id = 0;
      for (auto const& it : source_.colors_) {
        colors_string2id_[it.first] = id;
        colors_id2string_[id] = it.first;
        id++;
      }
      if (num_colors_ != id) {
        ROS_ERROR_STREAM (" ERROR EXTRACTED A WRONG NUMBER OF COLORS, extracted " << id << " instead of " << num_colors_);
        ros::shutdown();
      }
    }

    /**
     * @brief Computes the backward and forward propagation (and incidence) matrix using the arcs from the xml parsed data
     */
    void extractPrePosMatrix()
    {
      for (auto const& it : source_.arcs_) {
        int sourcefound = 0;
        Place::Id p_id;
        auto it_p = place_string2id_.find (it.second.sources_[0]);
        if (it_p != place_string2id_.end()) {
          p_id = it_p->second;
          sourcefound = 1;
        }
        else {
          it_p = place_string2id_.find (it.second.targets_[0]);
          if (it_p != place_string2id_.end()) {
            p_id = it_p->second;
          }
          else {
            ROS_ERROR_STREAM ("SOURCE OR TARGET OF THE ARC IS NOT A PLACE, OR DONT EXIST");
            ros::shutdown();
          }
        }

        Transition::Id t_id;
        if (sourcefound) {
          auto it_t = transition_string2id_.find (it.second.targets_[0]);
          if (it_t != transition_string2id_.end()) {
            t_id = it_t->second;
          }
          else {
            ROS_ERROR_STREAM ("TARGET OF THE ARC IS NOT A TRANSITION AS IT SHOULD BE, OR DONT EXIST");
            ros::shutdown();
          }
        }
        else {
          auto it_t = transition_string2id_.find (it.second.sources_[0]);
          if (it_t != transition_string2id_.end()) {
            t_id = it_t->second;
          }
          else {
            ROS_ERROR_STREAM ("SOURCE OF THE ARC IS NOT A TRANSITION AS IT SHOULD BE, OR DONT EXIST");
            ros::shutdown();
          }
        }

        Cost cost;
        for (auto const& iter : it.second.cost_) {
          auto it_c = colors_string2id_.find (iter.first);
          if (it_c != colors_string2id_.end()) {
            cost.insert (Cost::value_type (it_c->second, iter.second));
          }
          else {
            ROS_ERROR_STREAM ("ERRO COlOR");
            ros::shutdown();
          }
        }

        if (sourcefound) { //place is source, information goes to preMatrix - else transition is source, information goes to posMatrix
          backwards_matrix_[t_id][p_id] = cost;
        }
        else {
          forwards_matrix_[t_id][p_id] = cost;
        }
      }

      std::vector<int> au1 (num_colors_, 0);
      std::vector<std::vector<int> > aux1 (num_transitions_, au1);
      std::vector<std::vector<std::vector<int> > > temp1 (num_places_, aux1);
      incidence_matrix_ = temp1;
      for (size_t i = 0; i < num_places_ ; ++i) {
        for (size_t j = 0; j < num_transitions_ ; ++j) {
          for (size_t k = 0; k < num_colors_ ; ++k) {
            incidence_matrix_[i][j][k] = forwards_matrix_[j][i][k] - backwards_matrix_[j][i][k];
          }
        }
      }
    }

    /**
     * @brief Extract the initial marking from the parsed data to the PetriNet Execution format
     */
    void initMarking()
    {
      for (auto const& iter : source_.initial_marking_) {
        // if predicate then will not load the value from the parsed data
        if (predicates_.find (placeId2Id (iter.first)) != predicates_.end()) {
          continue;
        }
        Cost temp;
        for (auto const& it : iter.second) {
          temp[colorsId2Id (it.first)] = it.second;
        }
        if (!temp.empty()) {
          init_marking_[placeId2Id (iter.first)] = temp;
        }
      }
    }

  public:

    /* map between the primitive action manager ids and the petrinet ids */
    std::multimap<uint32_t, Place::Id > pa_id2pn_id_;
    std::map<Place::Id, uint32_t > pn_id2pa_id_;

    /* map between the predicate manager ids and the petrinet ids */
    std::multimap<uint32_t, Place::Id > pm_id2pn_id_;
    std::map<Place::Id, uint32_t > pn_id2pm_id_;

    std::set<Place::Id> actions_;
    std::set<Place::Id> predicates_;
    std::set<Place::Id> tasks_;
    std::set<Place::Id> regular_;

    std::map<Transition::Id, std::map<Place::Id, Cost > > backwards_matrix_;
    std::map<Transition::Id, std::map<Place::Id, Cost > > forwards_matrix_;
    std::vector<std::vector< std::vector< int> > > incidence_matrix_;

    std::map<Parser::Place::Id, Place::Id> place_string2id_;
    std::vector<Parser::Place::Id> place_id2string_;
    std::vector<std::string> place_id2name_;

    std::map<Parser::Transition::Id, Transition::Id> transition_string2id_;
    std::vector<Parser::Transition::Id> transition_id2string_;

    std::map<Parser::Color::Id, Color::Id> colors_string2id_;
    std::vector<Parser::Color::Id> colors_id2string_;

    std::map<Place::Id, Cost> init_marking_;

    size_t num_places_;
    size_t num_transitions_;
    size_t num_colors_;

    /**
     * @brief constructor
     *
     * TODO:: change description to a good one...
     * The PetriNet Independent class is used to store all immutable data used on the petrinet execution,
     * this class is also responsible to extract the data from the parsed data to a more simple data format
     *
     * @param filename petrinet filename
     */
    PetriNetStructure (const std::string& filename) :
      filename_ (filename),
      source_ (filename)
    {
      extractPlaces();
      extractTransitions();
      extractColors();
      extractPrePosMatrix();
      initMarking();
      printMatrices();
      printInitMarking();
    };

    ~PetriNetStructure() {};

    bool pmMapCallback (const std::map<std::string, uint32_t>& map)
    {
      std::multimap<uint32_t, Place::Id > temp;
      std::map<Place::Id, uint32_t > temp_rv;

      bool load = true;

      for (auto const& predicate : predicates_) {
        auto pm_it = map.find (placeId2Name (predicate));
        if (pm_it == map.end()) {
          if (placeId2Name (predicate).substr (0, 4) == "NOT_") {
            pm_it = map.find (placeId2Name (predicate).substr (4));
            if (pm_it == map.end()) {
              ROS_WARN_STREAM ("The predicate " << placeId2Name (predicate).substr (4) << " is needed by the petrinet, " << filename_ << ", and is not declared in the predicate manager.");
              load = false;
            }
            else {
              temp.insert (std::pair<uint32_t, Place::Id> (pm_it->second, predicate));
              temp_rv[predicate] = pm_it->second;
            }
          }
          else {
            ROS_WARN_STREAM ("The predicate " << placeId2Name (predicate) << " is needed by the petrinet, " << filename_ << ", and is not declared in the predicate manager.");
            load = false;
          }
        }
        else {
          temp.insert (std::pair<uint32_t, Place::Id> (pm_it->second, predicate));
          temp_rv[predicate] = pm_it->second;
        }
      }

      if (!load) {
        ROS_WARN_STREAM ("Did not load the predicate manager map, please correct the issues and publish the correct map");
        return false;
      }

      pm_id2pn_id_.clear();
      pn_id2pm_id_.clear();

      pm_id2pn_id_ = temp;
      pn_id2pm_id_ = temp_rv;

      ROS_INFO_STREAM ("Predicate Manager Map loaded and ready to run");
      printPredicate2PN();
      return true;
    }

    bool paMapCallback (const std::map<std::string, uint32_t>& map)
    {
      std::multimap<uint32_t, Place::Id > temp;
      std::map<Place::Id, uint32_t > temp_rv;

      bool load = true;
      if (actions_.empty()) {
        ROS_WARN_STREAM ("The petrinet " << filename_ << " dont have actions");
        return false;
      }

      for (auto const& action : actions_) {
        auto pa_it = map.find (placeId2Name (action));
        if (pa_it == map.end()) {
          ROS_WARN_STREAM ("The action " << placeId2Name (action) << " is needed by the petrinet, " << filename_ << ", and is not declared in the primitive action manager.");
          load = false;
        }
        else {
          temp.insert (std::pair<uint32_t, Place::Id> (pa_it->second, action));
          temp_rv[action] = pa_it->second;
        }
      }

      if (!load) {
        ROS_WARN_STREAM ("Did not load the primitive action manager map, please correct the issues and publish the correct map");
        return false;
      }

      pa_id2pn_id_.clear();
      pn_id2pa_id_.clear();

      pa_id2pn_id_ = temp;
      pn_id2pa_id_ = temp_rv;

      ROS_INFO_STREAM ("Primitive Action Manager Map loaded");
      printAction2PN();

      return true;
    }

    std::string getName()
    {
      return filename_;
    }

    /**
     * @brief Gives the Place name given the petrinet place id
     *
     * @param id the id of the petrinet place
     * @return a string with the place name
     */
    std::string placeId2Name (Place::Id id)
    {
      return place_id2name_[id];
    }

    /**
     * @brief Gives the XML Place Id given the petrinet place id
     *
     * @param id the id of the petrinet place
     * @return the xml place id
     */
    Parser::Place::Id placeId2Id (Place::Id id)
    {
      return place_id2string_[id];
    }

    /**
     * @brief Gives the petrinet place id given the xml place id
     *
     * @param id the xml place id
     * @return the petrinet place id
     */
    Place::Id placeId2Id (Parser::Place::Id id)
    {
      return place_string2id_.find (id)->second;
    }

    /**
     * @brief Gives the XML transition Id given the petrinet transition id
     *
     * @param id the id of the petrinet transition
     * @return the xml transition id
     */
    Parser::Transition::Id transitionId2Id (Transition::Id id)
    {
      return transition_id2string_[id];
    }

    /**
     * @brief Gives the petrinet transition id given the xml transition id
     *
     * @param id the xml transition id
     * @return the petrinet transition id
     */
    Transition::Id transitionId2Id (Parser::Transition::Id id)
    {
      return transition_string2id_.find (id)->second;
    }

    /**
     * @brief Gives the XML token/color Id given the petrinet token/color id
     *
     * @param id the id of the petrinet token/color
     * @return the xml token/color id
     */
    Parser::Color::Id colorsId2Id (Color::Id id)
    {
      return colors_id2string_[id];
    }

    /**
     * @brief Gives the petrinet token/color id given the xml token/color id
     *
     * @param id the xml token/color id
     * @return the petrinet token/color id
     */
    Color::Id colorsId2Id (Parser::Color::Id id)
    {
      return colors_string2id_.find (id)->second;
    }

    /**
     * @brief Prints the name of the action and the map between the Action Manager ids and the PetriNet ids
     */
    void printAction2PN()
    {
      for (auto const& id : pa_id2pn_id_) {
        ROS_INFO_STREAM ("PN_Id: " << id.second << " PA_ID: " << id.first << " NAME: " << placeId2Name (id.second));
      }
    }

    /**
     * @brief Prints the name of the predicate and the map between the Predicate Manager ids and the PetriNet ids
     */
    void printPredicate2PN()
    {
      for (auto const& id : pm_id2pn_id_) {
        ROS_INFO_STREAM ("PN_Id: " << id.second << " PM_ID: " << id.first << " NAME: " << placeId2Name (id.second));
      }
    }

    /**
     * @brief Prints the backwards, forwards and incidence propagation matrixes
     */
    void printMatrices()
    {
      for (size_t c_it = 0; c_it < num_colors_; ++c_it) {
        //          std::cout << "Color: " << colors_id2id (c_it) << "\n";
        std::cout << "\t\t\t PreM \t\t PostM \t\t iM\n";
        for (size_t p_it = 0; p_it < num_places_; ++p_it) {
          std::cout << /*place_id2name_[p_it] <<*/ "\t\t";
          for (size_t t_it = 0; t_it < num_transitions_; ++t_it) {
            std::cout << backwards_matrix_[t_it][p_it][c_it] << " ";
          }
          std::cout << "\t\t";
          for (size_t t_it = 0; t_it < num_transitions_; ++t_it) {
            std::cout << forwards_matrix_[t_it][p_it][c_it] << " ";
          }
          std::cout << "\t\t";
          for (size_t t_it = 0; t_it < num_transitions_; ++t_it) {
            std::cout << incidence_matrix_[p_it][t_it][c_it] << " ";
          }
          std::cout << "\n";
        }
      }
    }

    /**
     * @brief prints all action places
     */
    void printActions()
    {
      ROS_INFO_STREAM ("Actions:");
      for (auto const& it : actions_) {
        ROS_INFO_STREAM ("\tName: " << placeId2Name (it) << "\tParser ID: " << placeId2Id (it) << "\tPetri Net ID: " << it);
      }
    }

    /**
     * @brief prints all task places
     */
    void printTasks()
    {
      ROS_INFO_STREAM ("Tasks:");
      for (auto const& it : tasks_) {
        ROS_INFO_STREAM ("\tName: " << placeId2Name (it) << "\tParser ID: " << placeId2Id (it) << "\tPetri Net ID: " << it);
      }
    }

    /**
     * @brief prints all predicate places
     */
    void printPredicates()
    {
      ROS_INFO_STREAM ("Predicates:");
      for (auto const& it : predicates_) {
        ROS_INFO_STREAM ("\tName: " << placeId2Name (it) << "\tParser ID: " << placeId2Id (it) << "\tPetri Net ID: " << it);
      }
    }

    /**
     * @brief prints all regular places
     */
    void printRegular()
    {
      ROS_INFO_STREAM ("Regular:");
      for (auto const& it : regular_) {
        ROS_INFO_STREAM ("\tName: " << placeId2Name (it) << "\tParser ID: " << placeId2Id (it) << "\tPetri Net ID: " << it);
      }
    }

    /**
     * @brief prints the initial marking
     */
    void printInitMarking()
    {
      ROS_INFO_STREAM ("Marking:");
      for (auto const& iter : init_marking_) {
        //if (actions_.count (iter.first) != 0) {
        ROS_INFO_STREAM ("\tPLACE: " << placeId2Id (iter.first));
        for (auto const& it : iter.second) {
          ROS_INFO_STREAM (/*"\t\tCOLOR: " << colors_id2id (it.first) << */" Value: " << it.second);
        }
        //}
      }
    }

    /**
     * @brief prints all transitions
     */
    void printTransitions()
    {
      ROS_INFO_STREAM ("Transition:");
      for (size_t it = 0; it != num_transitions_; ++it) {
        ROS_INFO_STREAM ("\tName: " << transition_id2string_[it] << " ID: " << it);
      }
    }

    /**
     * @brief prints all tokens/colors
     */
    void printColors()
    {
      ROS_INFO_STREAM ("Colors:");
      for (size_t it = 0; it != num_colors_; ++it) {
        ROS_INFO_STREAM ("\tName: " << colors_id2string_[it] << " ID: " << it);
      }
    }

    /**
     * @brief prints all places
     */
    void printPlaces()
    {
      ROS_INFO_STREAM ("Place:");
      for (size_t it = 0; it != num_places_; ++it) {
        ROS_INFO_STREAM ("\tName: " << placeId2Id (it) << "\tParser ID: " << place_id2string_[it] << "\tPetri Net ID: " << it);
      }
    }

    /**
     * @brief Prints one Cost
     *
     * @param cost Cost
     */
    void printCost (Cost& cost)
    {
      std::cout << " - ";
      for (auto const& it : cost) {
        std::cout << it.second;
      }
      std::cout << " - ";
    }
};

#endif
