# README #

This repository contains the source code of the Petri Net Robotic Task Execution (PN-RTE) framework developed during my Master thesis.
The framework is a set of ROS packages capable of execution a Petri net model that represents a robotic task plan and the actions presente on each task based on the [H. Costelha and P. Lima. Robot task plan representation by petri nets: modelling,identification, analysis and execution. Autonomous Robots, 2012](http://link.springer.com/article/10.1007%2Fs10514-012-9288-x)

Please visit the wiki page at () for more details.

### Dependencies ###

The framework depends on the [Predicate Manager package](https://github.com/larsys/markov_decision_making/tree/master/predicate_manager) from the [Markov Decision Making metapackage](https://github.com/larsys/markov_decision_making)

### TODO ###